import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'RegFlutter',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('Register'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.person_pin),
                onPressed: () {},
              ),
            ],
          ),
          // body: SafeArea(),
          bottomNavigationBar: bottomAppBar,
        ),
      ),
    );
  }
}

BottomAppBar bottomAppBar = BottomAppBar(
  color: Colors.blue,
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Row(
        children: <Widget>[
          IconButton(
              icon: Icon(
                Icons.home,
                color: Colors.white,
              ),
              onPressed: () {}),
          IconButton(
              icon: Icon(
                Icons.mode_edit,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),
      IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.white,
          ),
          onPressed: () {}),
    ],
  ),
);
